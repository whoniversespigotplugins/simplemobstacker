package eu.whoniverse.spigot.simplemobstacker;

import eu.whoniverse.spigot.simplemobstacker.events.MobSpawnEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class SimpleMobStacker extends JavaPlugin {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new MobSpawnEvent(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
