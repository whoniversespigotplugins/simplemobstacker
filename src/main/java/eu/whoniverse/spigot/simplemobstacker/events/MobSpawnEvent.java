package eu.whoniverse.spigot.simplemobstacker.events;

import eu.whoniverse.spigot.simplemobstacker.SimpleMobStacker;
import eu.whoniverse.spigot.simplemobstacker.utils.References;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

public class MobSpawnEvent implements Listener {

    @EventHandler
    public void onMobSpawn(SpawnerSpawnEvent spawnerSpawnEvent) {
        AtomicReference<Integer> i = new AtomicReference<>(1);
        System.out.println(spawnerSpawnEvent.getEntity().getNearbyEntities(10, 10, 10).size());
        spawnerSpawnEvent.getEntity().getNearbyEntities(10, 10, 10).forEach(entity -> {
            if (entity.getType().equals(spawnerSpawnEvent.getEntityType())) {
                if (!entity.getMetadata(References.STACKED_MOB_AMOUNT).isEmpty()) {
                    entity.getMetadata(References.STACKED_MOB_AMOUNT).forEach(metadataValue -> {
                        if (Objects.requireNonNull(metadataValue.getOwningPlugin()).getClass().equals(SimpleMobStacker.class)) {
                            System.out.println("Found entity with " + metadataValue.asInt() + " Actual value is " + i.get());
                            i.set(metadataValue.asInt() + 1);
                        }
                    });
                } else {
                    i.set(i.get() + 1);
                }
                entity.remove();

            }
        });
        System.out.println("at the end " + i.get());
        spawnerSpawnEvent.getEntity().setMetadata(References.STACKED_MOB_AMOUNT, new FixedMetadataValue(JavaPlugin.getPlugin(SimpleMobStacker.class), i.get()));
        spawnerSpawnEvent.getEntity().setCustomName("x" + i.get());
        spawnerSpawnEvent.getEntity().setCustomNameVisible(true);

    }
}